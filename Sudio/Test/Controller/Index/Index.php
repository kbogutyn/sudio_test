<?php declare(strict_types=1);

namespace Sudio\Test\Controller\Index;

use Magento\Checkout\Model\Session;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\Controller\Result\JsonFactory;

class Index extends Action
{
    private $jsonFactory;
    private $session;
    private $request;

    public function __construct(
        Context $context,
        JsonFactory $jsonFactory,
        RequestInterface $request,
        Session $session
    ) {
        parent::__construct($context);
        $this->jsonFactory = $jsonFactory;
        $this->session = $session;
        $this->request = $request;
    }

    public function execute()
    {
        $pixel = $this->request->getParam('pixel');
        $result = $this->jsonFactory->create();

        if ($pixel !== $this->session->getData('pixel')) {
            $this->session->setData('pixel', $pixel);
        }
        $result->setData(['OK' => $pixel]);

        return $result;
    }
}
