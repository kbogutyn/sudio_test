<?php declare(strict_types=1);

namespace Sudio\Test\Observer;

use Magento\Checkout\Model\Session as CheckoutSession;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Quote\Model\QuoteRepository;
use Psr\Log\LoggerInterface;

class AddPixelToQuote implements ObserverInterface
{

    private $logger;
    private $checkoutSession;
    private $quoteRepository;

    public function __construct(
        CheckoutSession $checkoutSession,
        QuoteRepository $quoteRepository,
        LoggerInterface $logger
    ) {
        $this->checkoutSession = $checkoutSession;
        $this->quoteRepository = $quoteRepository;
        $this->logger = $logger;
    }

    public function execute(Observer $observer)
    {
        try {
            $quote = $this->checkoutSession->getQuote();
            $quote->setData('pixel', $this->checkoutSession->getData('pixel'));
            $this->quoteRepository->save($quote);
        } catch (\Throwable $exception) {
            $this->logger->critical('Unable to save quote pixel value', ['exception' => $exception, 'quote' => $quote]);
        }
    }
}
