<?php declare(strict_types=1);

namespace Sudio\Test\Observer;

use Magento\Checkout\Model\Session;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Sales\Model\OrderRepository;
use Psr\Log\LoggerInterface;

class AddPixelToOrder implements ObserverInterface
{

    private $orderRepository;
    private $checkoutSession;
    private $logger;

    public function __construct(
        OrderRepository $orderRepository,
        Session $checkoutSession,
        LoggerInterface $logger
    ) {
        $this->orderRepository = $orderRepository;
        $this->checkoutSession = $checkoutSession;
        $this->logger = $logger;
    }

    public function execute(Observer $observer)
    {
        $order = $observer->getData('order');
        try {
            $quote = $this->checkoutSession->getQuote();
            $pixel = $quote->getData('pixel');
            if ($pixel) {
                $order->setData('pixel', $pixel);
                $this->orderRepository->save($order);
            }
        } catch (\Throwable $exception) {
            $this->logger->critical('Unable to save order pixel value', ['exception' => $exception, 'order' => $order]);
        }
    }
}
