const urlParams = new URLSearchParams(window.location.search);
const pixel = urlParams.get('pixel');
const params = 'pixel=' + pixel;

const xmlHttp = new XMLHttpRequest();

xmlHttp.onreadystatechange = function() {
    if (xmlHttp.readyState === XMLHttpRequest.DONE) {
        if (xmlHttp.status === 200) {
            localStorage.setItem('pixel', pixel);
        }
    }
};

if (pixel && pixel!== localStorage.getItem('pixel')) {
    xmlHttp.open("GET", window.location.origin + "/pixel?" + params, true);
    xmlHttp.send();
}
